package model;


public class GenBounded implements GenerationStrategy{
    private double debut;

    private double fin;


    @Override
    public double generate(){
        return  (Math.random()*(this.fin-this.debut+1)+this.debut);
    }

    public GenBounded(){
        this.debut=(Math.random()*(49-1+1)+1);
        this.fin=(Math.random()*(100-50+1)+50);
    }

    public GenBounded(int debut, int fin){
        this.debut=debut;
        this.fin=fin;
    }
}

