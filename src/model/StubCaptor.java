package model;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.beans.property.MapProperty;
import javafx.beans.property.SimpleMapProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import javafx.scene.control.TreeItem;

import java.io.*;
import java.util.Arrays;
import java.util.List;


public class StubCaptor implements Serializable {


    private static final long serialVersionUID = 1L;

    private  final String fileName = "capteurs.bin";

    private ObservableMap<Captor, Double> captorsObs = FXCollections.observableHashMap();
    private MapProperty<Captor, Double> captors = new SimpleMapProperty<>(captorsObs);

    private ObservableMap<Captor, List<Captor>> parents = FXCollections.observableHashMap();

    public StubCaptor() throws IOException {
        // Charger les données à partir du fichier JSON s'il existe
        try (FileInputStream fileIn = new FileInputStream(fileName);
             ObjectInputStream in = new ObjectInputStream(fileIn)) {
            StubCaptor data = (StubCaptor) in.readObject();
            this.parents = data.parents;
        } catch (IOException | ClassNotFoundException e) {
            Captor c1 = new CaptorBasique("Captor1");
            Captor c2 = new CaptorBasique("Captor2");
            ObservableMap<Captor, Double> captors = FXCollections.observableHashMap();
            captors.put(c1, 0.5);
            captors.put(c2, 0.5);
            Captor c3 = new CaptorBasique("Captor3");
            Captor c4 = new CaptorBasique("Captor4");
            ObservableMap<Captor, Double> captorsSecond = FXCollections.observableHashMap();
            captorsSecond.put(c3, 0.5);
            captorsSecond.put(c4, 0.5);
            // Initialiser les données par défaut si le fichier JSON n'existe pas
            parents.put(new CaptorZone("Zone1", captors), Arrays.asList(
                    c1,
                    c2));
            parents.put(new CaptorZone("Zone2", captorsSecond), Arrays.asList(
                    c3,
                    c4));
        }
    }

    public MapProperty<Captor, List<Captor>> getParents(){
        return new SimpleMapProperty<>(parents);
    }

    public ObservableMap<Captor, List<Captor>> getCaptors(){
        return this.parents;
    }

    public void saveToJsonFile(ObservableMap<Captor, List<Captor>> captors) throws IOException {
        try (FileOutputStream fileOut = new FileOutputStream(fileName);
             ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
            out.writeObject(captors);
        }
    }
}
