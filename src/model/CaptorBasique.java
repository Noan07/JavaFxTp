package model;

import javafx.application.Platform;

public class CaptorBasique extends Captor implements Runnable{
    private int timeGenrate = 1000;
    private Thread thread;
    public CaptorBasique(String name) {
        super(name);
        this.startThread();
    }

    public CaptorBasique(String name, GenerationStrategy gen) {
        super(name, gen);
        this.startThread();
    }
    public void startThread(){
        thread=new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }
    @Override
    public void run() {
        while (true){
            try {
                Platform.runLater(()->super.setTemperature(super.genStrat.generate()) );
                Thread.sleep(timeGenrate);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
