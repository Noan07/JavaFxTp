package model;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;

import java.util.Map;

public class CaptorZone extends Captor{
    private  ObservableMap<Captor, Double> captorTemperatures;
    private  Thread thread;
    public CaptorZone(String name, ObservableMap<Captor, Double> captorTemperatures) {
        super(name);
        this.captorTemperatures = captorTemperatures;
        this.startThread();
    }

    public CaptorZone(String name) {
        super(name);
        this.startThread();
    }





    private double calculateTemperatureMean() {
        double sum = 0;
        for (double temperature : captorTemperatures.values()) {
            sum += temperature;
        }
        return sum / captorTemperatures.size();
    }

    public void startThread(){
        thread = new Thread(this);
        thread.setDaemon(true);
        thread.start();
    }



    @Override
    public void run() {
        while (true){
            try {
                float totalTemp =0;
                if (captorTemperatures.size()!=0){
                    for (Map.Entry<Captor, Double> entry : captorTemperatures.entrySet()) {
                        totalTemp += entry.getKey().getTemperature() * entry.getValue();
                    }
                }
                totalTemp=totalTemp/captorTemperatures.size();
                float finalTotalTemp = totalTemp;
                Platform.runLater(()->super.setTemperature(finalTotalTemp));
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
