package model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.UUID;

public abstract class Captor extends Observable implements Runnable, Serializable {
    private final ObjectProperty<UUID> id = new SimpleObjectProperty<>(UUID.randomUUID());
    private StringProperty nameProperty = new SimpleStringProperty();
    private double temperature;
    protected GenerationStrategy genStrat;
    private double time = 3000;

    public Captor(String name){
        this.nameProperty.set(name);
        this.genStrat = new GenBounded();
    }

    public Captor(String name, GenerationStrategy gen){
        this.nameProperty.set(name);
        this.genStrat=gen;
    }

    public ObjectProperty<UUID> idProperty() {
        return id;
    }

    public StringProperty nameProperty() {
        return nameProperty;
    }

    public String getName() {
        return nameProperty().get();
    }

    public double getTime() {return time;}

    public void setTime(double time) {this.time=time; }

    public void setName(String name) {
        this.nameProperty().set(name);
    }

    public double getTemperature() {
        return this.temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
        this.notifier();
    }

    public void setStratey(GenerationStrategy s){
        this.genStrat=s;
    }

    @Override
    public void run() {}

    @Override
    public String toString() {
        return nameProperty.get();
    }

}