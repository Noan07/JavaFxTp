package model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class GenCPU implements GenerationStrategy{
    @Override
    public double generate() {
        // /sys/class/thermal_zone2 cat temp
        double temp = 0;
        try {
            FileInputStream file = new FileInputStream("/sys/class/thermal/thermal_zone2/temp");
            Scanner scanner = new Scanner(file);

            while (scanner.hasNextLine()) {
                temp = Float.parseFloat(scanner.nextLine());
            }
            scanner.close();
            temp = temp/1000;
        } catch (FileNotFoundException f){
            return 0;
        }
        return temp;
    }
}
