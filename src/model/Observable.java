package model;

import java.util.ArrayList;
import java.util.List;

public abstract class Observable {
    private List<ObservableCaptor> observables = new ArrayList<>();
    public void attacher(ObservableCaptor o){
        this.observables.add(o);
    }
    public void detacher(ObservableCaptor o){
        this.observables.remove(o);
    }
    public void notifier(){
        observables.forEach(ObservableCaptor::update);
    }
}
