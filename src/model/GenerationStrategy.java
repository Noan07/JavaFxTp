package model;

public interface GenerationStrategy {

    public default double generate(){
        return 0;
    }
}
