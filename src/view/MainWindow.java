package view;



import javafx.beans.binding.Bindings;
import javafx.collections.ObservableMap;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.*;

import javax.imageio.IIOParam;
import java.io.IOException;
import java.util.*;


public class MainWindow {
    @FXML
    private BorderPane mainBorderPane;

    private ObservableMap<Captor, List<Captor>> parents;

    @FXML
    private Button button;

    @FXML
    private Label childrenNamesLabel;

    @FXML
    private Label detailLabel;
    @FXML
    private Label detailUid;

    @FXML
    private Button addChildButton;
    @FXML
    private TreeView<Captor> ListCaptors;
    @FXML
    private TextField detailTextField ;
    @FXML
    private SplitPane splitPane;


    @FXML
    private  void initialize() throws IOException {
        StubCaptor Stub = new StubCaptor();
        parents = Stub.getParents();
        TreeItem<Captor> root = new TreeItem<>();
        root.setExpanded(true);
        for (Map.Entry<Captor, List<Captor>> entry : Stub.getParents().entrySet()) {
            ImageView icon = new ImageView(new Image(getClass().getResourceAsStream("/image/imageZone.jpg")));
            icon.setFitWidth(50);
            icon.setPreserveRatio(true);
            TreeItem<Captor> parent = new TreeItem<Captor>(entry.getKey(),icon);
            for (Captor child : entry.getValue()) {
                ImageView icon1 = new ImageView(new Image(getClass().getResourceAsStream("/image/tempC.jpg")));
                icon1.setFitWidth(50);
                icon1.setPreserveRatio(true);
                parent.getChildren().add(new TreeItem<Captor>(child,icon1));
            }
            root.getChildren().add(parent);
        }
        ListCaptors.setRoot(root);
        ListCaptors.setShowRoot(false);
        ListCaptors.getSelectionModel().select(0);
        ListCaptors.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null && !newValue.isLeaf()){
                StringBuilder childrenName = new StringBuilder();
                for (TreeItem<Captor> child : newValue.getChildren()) {
                    childrenName.append(child.getValue().getName() + ",");
                }
                childrenNamesLabel.setText(childrenName.toString());
                childrenNamesLabel.setVisible(true);
                detailLabel.setVisible(true);
                addChildButton.setVisible(true);
            }else{
                addChildButton.setVisible(false);
                childrenNamesLabel.setVisible(false);
                detailLabel.setVisible(false);
            }
        });

        ListCaptors.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null){
                detailUid.textProperty().bind(newValue.getValue().idProperty().asString());
                if(oldValue != null){
                    detailTextField.textProperty().unbindBidirectional(oldValue.getValue().nameProperty());
                }
                detailTextField.textProperty().bindBidirectional(newValue.getValue().nameProperty());
                newValue.getValue().nameProperty().addListener((observable1, oldValue1, newValue1) -> {
                    ListCaptors.refresh();
                });
            }else{
                detailUid.textProperty().unbind();
                detailUid.setText("");
            }
        });
    }

    @FXML
    private void handleButtonClickTemp(ActionEvent event) {
        TreeItem<Captor> selectedItem = ListCaptors.getSelectionModel().getSelectedItem();
        if (selectedItem != null) {
            Captor selectedCaptor = selectedItem.getValue();
            try {
                CaptorTemperatureController controller = new CaptorTemperatureController(selectedCaptor);
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/DisplayTemp.fxml"));
                fxmlLoader.setController(controller);
                Parent rootDisplayTemp = fxmlLoader.load();
                Scene scene = new Scene(rootDisplayTemp);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void handleButtonClick(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/AddCaptor.fxml"));
        Parent root = fxmlLoader.load();
        AddCaptor addCaptor = fxmlLoader.getController();
        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.showAndWait();
        String newValue = addCaptor.getNewValue();
        String value = addCaptor.getStrategy();
        System.out.println(value);
        Captor newCaptor;
        if(Objects.equals(value, "cpu")){
            newCaptor = new CaptorBasique(newValue, new GenCPU());
        } else if (Objects.equals(value, "zone")) {
            newCaptor = new CaptorZone(value);
        }else{
            newCaptor = new CaptorBasique(newValue,new GenBounded());
        }
        parents.put(newCaptor, new ArrayList<>());
        ListCaptors.setRoot(null);
        TreeItem<Captor> tr = new TreeItem<>();
        tr.setExpanded(true);
        for (Map.Entry<Captor, List<Captor>> entry : parents.entrySet()) {
            ImageView icon = new ImageView(new Image(getClass().getResourceAsStream("/image/imageZone.jpg")));
            icon.setFitWidth(50);
            icon.setPreserveRatio(true);
            TreeItem<Captor> parent = new TreeItem<Captor>(entry.getKey(),icon);
            for (Captor child : entry.getValue()) {
                ImageView icon1 = new ImageView(new Image(getClass().getResourceAsStream("/image/tempC.jpg")));
                icon1.setFitWidth(50);
                icon1.setPreserveRatio(true);
                parent.getChildren().add(new TreeItem<Captor>(child,icon1));
            }
            tr.getChildren().add(parent);
        }
        ListCaptors.setRoot(tr);
        ListCaptors.setShowRoot(false);
        splitPane.getItems().remove(detailTextField);
        splitPane.getItems().add(detailTextField);
    }

    @FXML
    private void handleButtonAddChild() {
        TreeItem<Captor> selectedItem = ListCaptors.getSelectionModel().getSelectedItem();
        if (selectedItem != null ) {
            // Create a dialog to get the child name
            TextInputDialog dialog = new TextInputDialog();
            dialog.setTitle("Add Child");
            dialog.setHeaderText("Enter the name of the new child:");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                Captor newChild = new CaptorBasique(result.get());
                //check if parent exist in the map
                if(parents.containsKey(selectedItem.getValue())) {
                    List<Captor> children = new ArrayList<>(parents.get(selectedItem.getValue()));
                    children.add(newChild);
                    parents.put(selectedItem.getValue(), Collections.unmodifiableList(children));
                }else {
                    List<Captor> children = new ArrayList<>();
                    children.add(newChild);
                    parents.put(selectedItem.getValue(), children);
                }
                //create treeitem for the new child and add it to the selected parent
                ImageView icon1 = new ImageView(new Image(getClass().getResourceAsStream("/image/tempC.jpg")));
                icon1.setFitWidth(50);
                icon1.setPreserveRatio(true);
                TreeItem<Captor> newChildTreeItem = new TreeItem<Captor>(newChild,icon1);
                selectedItem.getChildren().add(newChildTreeItem);
                ListCaptors.refresh();
            }
        }
    }
    @FXML
    private void handleButtonDelete(ActionEvent event) {
        TreeItem<Captor> selectedItem = ListCaptors.getSelectionModel().getSelectedItem();
        if (selectedItem != null && selectedItem.isLeaf()) {
            Captor captorToDelete = selectedItem.getValue();
            TreeItem<Captor> parent = selectedItem.getParent();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Captor");
            alert.setHeaderText("Delete Captor: " + captorToDelete.getName());
            alert.setContentText("Are you sure you want to delete this captor?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                parent.getChildren().remove(selectedItem);
                parent.getChildren().removeIf(item -> item.equals(selectedItem));
            }
        } else if (selectedItem != null ) {
            Captor captorToDelete = selectedItem.getValue();
            TreeItem<Captor> parent = selectedItem.getParent();
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Delete Captor");
            alert.setHeaderText("Delete Captor: " + captorToDelete.getName());
            alert.setContentText("Are you sure you want to delete this captor?");
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                if (selectedItem.isLeaf()) {
                    parent.getChildren().remove(selectedItem);
//update data
                    parent.getChildren().removeIf(item -> item.equals(selectedItem));
                } else {
                    parent.getChildren().remove(selectedItem);
                    parents.remove(captorToDelete);
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Error");
            alert.setContentText("Une erreur est survenue!");
        }
    }
}
