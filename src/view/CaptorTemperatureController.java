package view;

import javafx.beans.Observable;
import javafx.fxml.FXML;
import javafx.scene.chart.ScatterChart;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Captor;
import model.ObservableCaptor;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

public class CaptorTemperatureController implements ObservableCaptor {

    static private NavigableMap<Double, Image> imageNavigableMap = new TreeMap<>();
    @FXML
    private Label temperatureLabel;

    @FXML
    private ImageView imageview;
    private Captor captor;

    public void initialize(){
        imageNavigableMap.put(40.0, new Image("/image/brule.jpg"));
        imageNavigableMap.put(20.0, new Image("/image/soleil.jpg"));
        imageNavigableMap.put(0.0, new Image("/image/normal.jpg"));
        imageNavigableMap.put(-Double.MAX_VALUE, new Image("/image/froid.jpg"));
        setTemperature(captor.getTemperature());
        captor.attacher(this);
        update();
    }

    public void setTemperature(double temperature) {
            temperatureLabel.setText(String.valueOf(temperature));

    }

    public  CaptorTemperatureController(Captor selectedCaptor) {
        this.captor = selectedCaptor;
    }

    @Override
    public void update() {
        Map.Entry<Double, Image> entry = imageNavigableMap.floorEntry((double) captor.getTemperature());
        if(entry != null && entry.getValue() != null){
            imageview.setImage(entry.getValue());
        }
        temperatureLabel.setText(String.format("%.2fC",captor.getTemperature()));
    }


}