package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.stage.Stage;
import model.Captor;
import model.CaptorBasique;

public class AddCaptor {
    @FXML
    private TextField newValueField;
    private String newValue;
    private String strategy;
    @FXML
    private ComboBox<String> strategyChoice;

    @FXML
    private void onValidate(ActionEvent event) {
        newValue = newValueField.getText();
        strategy = strategyChoice.getValue();
        // utilisez la variable strategy pour définir la stratégie de génération
        Stage stage = (Stage) newValueField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onCancel(ActionEvent event) {
        Stage stage = (Stage) newValueField.getScene().getWindow();
        stage.close();
    }
    public String getNewValue() {
        return newValue;
    }
    public String getStrategy() {
        return strategy;
    }

}